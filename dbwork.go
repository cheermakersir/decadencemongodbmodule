package dbwork

import (
	"log"
	"io/ioutil"
	"fmt"
	"encoding/json"
	"strings"
	"strconv"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"context"
	"go.mongodb.org/mongo-driver/bson"
)

var (
	DBclient *mongo.Client
)

func initialiseDBclient () error {
	clientOptions := options.Client().ApplyURI("mongodb://localhost:27017")
	client, err := mongo.Connect(context.Background(), clientOptions)
	if err != nil {
		log.Fatal(err)
	}

	err = client.Ping(context.Background(), nil)
	if err != nil {
		log.Fatal(err)
	}

	DBclient = client

	log.Println("client created")

	return nil
}

func checkDatabaseAndCollection() {
	databases, err := DBclient.ListDatabaseNames(context.Background(), nil, nil)
	if err != nil {
		log.Println("Unable to get DBlist")
		log.Fatal(err)
	}

	if !containsString(databases, "TestDB") {
		log.Println("TestDB not found, creating a new database...")

		err = createDatabase()
		if err != nil {
			log.Fatal(err)
		}
	}

	database := DBclient.Database("TestDB")

	collections, err := database.ListCollectionNames(context.Background(), bson.D{})
	if err != nil {
		log.Fatal(err)
	}

	if !containsString(collections, "Persons") {
		log.Println("Persons collection not found, creating a new collection...")

		err = createCollection(database)
		if err != nil {
			log.Fatal(err)
		}
	}

	count, err := database.Collection("Persons").CountDocuments(context.Background(), bson.D{})
	if err != nil {
		log.Fatal(err)
	}

	log.Printf("Total documents in Persons collection: %d", count)
}

func createDatabase() error {
	err := DBclient.Database("TestDB").CreateCollection(context.Background(), "Persons", nil)
	if err != nil {
		return err
	}

	log.Println("TestDB created.")
	return nil
}

func createCollection(database *mongo.Database) error {
	err := database.CreateCollection(context.Background(), "Persons",nil)
	if err != nil {
		return err
	}

	log.Println("Persons collection created.")
	return nil
}

func containsString(slice []string, value string) bool {
	for _, item := range slice {
		if item == value {
			return true
		}
	}
	return false
}